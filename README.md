# Intelli Home Bean Conf
Progetto demo per il corso **Spring for Beginner**

All'interno di questo progetto, andremo a rendere concreto l'esempio disponibile sulle slide.
All'interno troveremo la cncretizzazione di come i bean possano essere usati
attraverso delle factory ed a sua volta, la stessa factory iniettata in un servizio.

Tratteremo:

* Factory Bean
* Java Configuration Bean
* Component Scan
* Component
* Services
* Autowiring
* Utilizzo dei services

L'esempio trattato fa riferimento alla prima lezione