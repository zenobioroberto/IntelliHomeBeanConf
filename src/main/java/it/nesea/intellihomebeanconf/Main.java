package it.nesea.intellihomebeanconf;

import it.nesea.intellihomebeanconf.components.ElectricPowerSwitch;
import it.nesea.intellihomebeanconf.configurations.AppConfig;
import it.nesea.intellihomebeanconf.services.SwitchableService;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {
    public static void main(String args[]){
        //AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(AppConfig.class);
        //Configurazione dell'application context attraverso l'annotation.
        AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();
        ctx.register(AppConfig.class);
        ctx.refresh(); //reload dell'application context

        ElectricPowerSwitch electricPowerSwitch = (ElectricPowerSwitch) ctx.getBean("electricPowerSwitch");
        electricPowerSwitch.press();

        for (String beanName : ctx.getBeanDefinitionNames()){
            System.out.println("beanName = " + beanName);
        }
        //Questi due metodi mostrano come ElectricPowerSwitch mantenga lo status poiché è un singleton
        ElectricPowerSwitch fanSwitch = (ElectricPowerSwitch) ctx.getBean("funSwitch");
        fanSwitch.press();

        SwitchableService switchableService = (SwitchableService) ctx.getBean("switchableService");
        switchableService.pressButton();
    }
}
