package it.nesea.intellihomebeanconf.devices;

public class Fan implements Switchable {
    public void turnOn() {
        System.out.println("Fan Start");
    }

    public void turnOff() {
        System.out.println("Fan Stop");
    }
}
