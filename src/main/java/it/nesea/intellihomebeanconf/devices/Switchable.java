package it.nesea.intellihomebeanconf.devices;

public interface Switchable {
    void turnOn();
    void turnOff();
}
