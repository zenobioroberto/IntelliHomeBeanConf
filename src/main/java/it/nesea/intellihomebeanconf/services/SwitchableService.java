package it.nesea.intellihomebeanconf.services;

import it.nesea.intellihomebeanconf.components.Switch;
import it.nesea.intellihomebeanconf.configurations.AppConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class SwitchableService {
    private Switch aSwitch;

    /**
     * Abbiamo utilizzato l'annotation @Qualifier.
     * Questa fa si che un bean venga identificato dal suo nome byName.
     * Guardando infatti la classe di configurazione @{@link AppConfig}
     * ci renderemo conto che c'è un bean esattamente chiamato così.
     *
     * PS: i nomi dei bean, se non definiti, sono dati dalla firma del metodo.
     * @param aSwitch
     */
    @Autowired
    @Qualifier("funSwitch")
    public void setaSwitch(Switch aSwitch) {
        this.aSwitch = aSwitch;
    }

    public void pressButton(){
        aSwitch.press();
    }
}
