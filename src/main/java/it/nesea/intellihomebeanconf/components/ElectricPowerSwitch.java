package it.nesea.intellihomebeanconf.components;

import it.nesea.intellihomebeanconf.devices.Switchable;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Component potremmo marcarlo come component, tuttavia
 * dobbiamo ricordare che questo richiederebbe il package in
 * component scan. Da quel momento il bean è presente nel container
 */
public class ElectricPowerSwitch implements Switch {

    private Switchable device;
    private boolean on;

    /**
     * Stiamo utilizzando l'autowired costructor. In particolare
     * ByType.
     * @param device
     */
    @Autowired
    public ElectricPowerSwitch(Switchable device) {
        this.device = device;
    }

    public boolean isOn() {
        return on;
    }

    public void press() {
        boolean checkOn = this.isOn();
        if (checkOn){
            device.turnOff();
            this.on = false;
        }else{
            device.turnOn();
            this.on = true;
        }

    }
}
