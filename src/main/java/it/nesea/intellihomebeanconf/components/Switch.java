package it.nesea.intellihomebeanconf.components;

public interface Switch {
    boolean isOn();

    void press();
}
