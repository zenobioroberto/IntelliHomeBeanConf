package it.nesea.intellihomebeanconf.configurations;

import it.nesea.intellihomebeanconf.components.ElectricPowerSwitch;
import it.nesea.intellihomebeanconf.devices.Fan;
import it.nesea.intellihomebeanconf.devices.Switchable;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration //segna il bean come configurazione
@ComponentScan(basePackages = {"it.nesea.intellihomebeanconf.services"}) //dice all'applicationContext di effettuare una scansione dei packages indicati cercando dei components
public class AppConfig {
    @Bean
    public ElectricPowerSwitch electricPowerSwitch(){
        return new ElectricPowerSwitch( new Fan());
    }

    @Bean()
    public ElectricPowerSwitch funSwitch(){
        return new ElectricPowerSwitch(fan());
    }

    @Bean
    public Switchable fan(){
        return new Fan();
    }
}
